﻿using System;
using System.Security.Cryptography.X509Certificates;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MathLogicLambdas;

namespace ParserTests
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void TestParsing()
        {
            Term e;

            //Test abstraction
            e = LambdaParser.Parse(@"\x.x");
            Assert.IsInstanceOfType(e, typeof(Abstraction));
            Assert.IsInstanceOfType(((Abstraction) e).AbstractTerm, typeof(Variable));
            Assert.AreEqual(((Variable)((Abstraction)e).AbstractTerm).Name, "x");

            //Test application
            e = LambdaParser.Parse(@"(\x.x) \x.x");
            Assert.IsInstanceOfType(e, typeof(Application));
            Assert.IsInstanceOfType(((Application)e).Left, typeof(Abstraction));
            Assert.IsInstanceOfType(((Application)e).Right, typeof(Abstraction));

            //Difficult parsing
            e = LambdaParser.Parse(@"\x.(\x.x x) (x x \x.x)");
            Assert.IsNotNull(e);
        }

        [TestMethod]
        public void TestToString()
        {
            var e = LambdaParser.Parse(@"\x.(\x.x x) (x x \x.x) \y.y");
            Assert.IsNotNull(e);
            Assert.AreEqual(e.ToString(), @"\x.(\x.x x) (x x \x.x) \y.y");

            e = LambdaParser.Parse(@"x x x x \x.x \x.x");
            Assert.AreEqual(e.ToString(), @"x x x x \x.x \x.x");
        }

        [TestMethod]
        public void TestIsNormal()
        {
            Term e = @"\x.(\x.x x) (x x \x.x) \y.y";
            Assert.IsFalse(e.IsNormal);

            e = @"(\x.y (x \x.y)) x";
            Assert.IsFalse(e.IsNormal);

            e = @"x \x.x";
            Assert.IsTrue(e.IsNormal);
        }

        [TestMethod]
        public void TestVariables()
        {
            Term e = @"\x.x";
            Assert.IsTrue(e.Variables.Count == 0);

            e = @"\x.y z";
            Assert.IsTrue(e.Variables.Count == 2);
            Assert.IsTrue(e.Variables.Contains(new Variable("y")));
            Assert.IsTrue(e.Variables.Contains(new Variable("z")));

            e = @"\x.\y.\z.x y z";
            Assert.IsTrue(e.Variables.Count == 0);

            e = @"(\x.\y.\z.m n) p";
            Assert.IsTrue(e.Variables.Count == 3);
            Assert.IsTrue(e.Variables.Contains(new Variable("m")));
            Assert.IsTrue(e.Variables.Contains(new Variable("n")));
            Assert.IsTrue(e.Variables.Contains(new Variable("p")));
        }

        [TestMethod]
        public void TestFreeForInline()
        {
            Term e = @"\x.x";
            Assert.IsTrue(e.IsFreeForInline("y", "x"));
            Assert.IsFalse(e.IsFreeForInline("x", "x"));

            e = @"\x.b \y.c d";
            Assert.IsTrue(e.IsFreeForInline(@"\x.x", "b"));
            Assert.IsTrue(e.IsFreeForInline(@"c", "d"));
            Assert.IsTrue(e.IsFreeForInline(@"y", "b"));
            Assert.IsFalse(e.IsFreeForInline(@"x y", "d"));
            Assert.IsFalse(e.IsFreeForInline(@"\y.x y", "b"));
        }

        [TestMethod]
        public void TestSubstitute()
        {
            Term e = @"\x.a x b";
            Term s = e.Substitute(new Variable("a"), @"\y.y a");
            Assert.AreEqual(s, @"\x.(\y.y a) x b");
            s = s.Substitute(new Variable("b"), @"\x.x");
            Assert.AreEqual(s, @"\x.(\y.y a) x \x.x");
            s = s.Substitute(new Variable("x"), "y");
            Assert.AreEqual(s, @"\x.(\y.y a) x \x.x");
            s = s.Substitute(new Variable("a"), "c");
            Assert.AreEqual(s, @"\x.(\y.y c) x \x.x");
        }

        [TestMethod]
        public void TestReduce()
        {
            Term e = @"(\x.y) x";
            Assert.AreEqual(e.Reduce(), "y");

            e = @"(\x.x) a";
            Term r = e.Reduce();
            Assert.AreEqual(r, "a");

            e = @"(\x.x) (\x.x)";
            r = e.Reduce();
            Assert.AreEqual(r, @"(\x.x)");

            e = @"(\x.x x) (\x.x x)";
            r = e.Reduce();
            Assert.IsNull(r);

            e = @"(\x.x) (\x.x) y";
            r = e.Reduce();
            Assert.AreEqual(r, @"(\x.x) y");
            r = r.Reduce();
            Assert.AreEqual(r, @"y");
        }

        [TestMethod]
        public void TestNormalize()
        {
            Term e = @"(\x.x) (\x.x) y";
            Term r = e.Normalize();
            Assert.AreEqual(r, "y");

            e = @"(\x.x) ((\x.x y) a)";
            r = e.Normalize();
            Assert.AreEqual(r, "a y");

            e = @"(\x.x x) (\x.x x)";
            r = e.Normalize();
            Assert.IsNull(r);

            e = @"(\x.x) ((\x.x) x)";
            r = e.Normalize();
            Assert.AreEqual(r, "x");
        }

        [TestMethod]
        public void TestKCI()
        {
            Term e = @"(\x.x) x";
            Assert.AreEqual(e.TransformKCI(), "I x");
            Assert.AreEqual(e.Normalize(), "x");

            e = @"(\f.\g.\x.f x (g x)) (\x.x) (\x.x) x";
            e = e.TransformKCI();
            Assert.AreEqual(e.Normalize(), "x x");


            e = @"(\x.\y.x) x y";
            e = e.TransformKCI();
            Assert.AreEqual(e.Normalize(), "x");
        }
    }
}
