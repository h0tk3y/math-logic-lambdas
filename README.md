Lambdas
=========
This code performs the following:
---------
- parsing non-typed lambdas;
- checking a lambda term for being in a normal form;
- checking a term for being free for substitution into an expression instead of a variable;
- substitution of a term into an expression instead of a variable
- beta-reducing a term in the normal reduction order
- normalizing a lambda term or coming to a conclusion of impossibility to normalize the term;