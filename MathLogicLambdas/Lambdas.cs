﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Configuration;
using System.Runtime.InteropServices;

namespace MathLogicLambdas
{
    public abstract class Term
    {
        private string _stringRepresentation;
        public abstract bool IsNormal { get; }
        protected abstract string StringRepresentation { get; }
        public abstract HashSet<Variable> Variables { get; }

        public static implicit operator Term(string s)
        {
            return LambdaParser.Parse(s);
        }

        public override string ToString()
        {
            return _stringRepresentation ?? (_stringRepresentation = StringRepresentation);
        }

        public override bool Equals(object obj)
        {
            return obj is Term && ToString().Equals(obj.ToString());
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public bool IsFreeForInline(Term t, Term insteadOf)
        {
            return CheckForInline(t, insteadOf, new Dictionary<Abstraction, Variable>());
        }

        protected internal bool CheckForInline(Term t, Term insteadOf,
            Dictionary<Abstraction, Variable> boundVariables)
        {
            if (Equals(insteadOf))
                return !t.Variables.Any(x => boundVariables.Any(y => y.Value.Equals(x)));
            return CheckForInlineImpl(t, insteadOf, boundVariables);
        }

        protected abstract bool CheckForInlineImpl(Term t, Term insteadOf,
            Dictionary<Abstraction, Variable> boundVariables);

        public Term Substitute(Variable t, Term with)
        {
            return SubstituteImpl(t, with, new Dictionary<Abstraction, Variable>()) ?? this;
        }

        public Term Reduce()
        {
            var result = ReduceImpl(new Dictionary<Abstraction, Variable>());
            return Equals(result) ? null : result;
        }

        public Term Normalize()
        {
            Term t = this;
            Term n;
            while ((n = t.Reduce()) != null)
                t = n;
            return Equals(t) || !t.IsNormal ? null : t;
        }

        public abstract Term TransformKCI();

        protected internal abstract Term ReduceImpl(Dictionary<Abstraction, Variable> boundVariables);

        protected internal abstract Term SubstituteImpl(Variable v, Term with, Dictionary<Abstraction, Variable> boundVariables);
    }

    public abstract class Combinator : Abstraction
    {
        protected Combinator(Variable variable, Term term) : base(variable, term) { }

        public override Term TransformKCI() { return this; }
    }

    public class KCombinator : Combinator
    {
        private KCombinator() : base("x", @"\y.x") { }

        public override string ToString() { return "K"; }

        public static readonly KCombinator K = new KCombinator();
    }

    public class SCombinator : Combinator
    {
        private SCombinator() : base("f", @"\g.\x.f x (g x)") { }

        public override string ToString() { return "S"; }

        public static readonly SCombinator S = new SCombinator();
    }

// ReSharper disable once InconsistentNaming
    public class ICombinator : Combinator
    {
        private ICombinator() : base("x", "x") { }

        public override string ToString() { return "I"; }

        public static readonly ICombinator I = new ICombinator(); 
    }

    public class Variable : Term
    {
        public Variable(string name)
        {
            Name = name;
        }

        public static implicit operator Variable(string s)
        {
            return new Variable(s);
        }

        public string Name { get; private set; }

        protected override string StringRepresentation
        {
            get { return Name; }
        }

        public override bool IsNormal
        {
            get { return true; }
        }

        public override HashSet<Variable> Variables
        {
            get { return new HashSet<Variable> {this}; }
        }

        protected override bool CheckForInlineImpl(Term t, Term insteadOf,
            Dictionary<Abstraction, Variable> boundVariables)
        {
            return true;
        }

        public override Term TransformKCI()
        {
            return this;
        }

        protected internal override Term ReduceImpl(Dictionary<Abstraction, Variable> boundVariables)
        {
            return this;
        }

        protected internal override Term SubstituteImpl(Variable v, Term with, Dictionary<Abstraction, Variable> boundVariables)
        {
            if (!Equals(v) || boundVariables.Values.Any(Equals))
                return this;
            return with;
        }
    }

    public class Abstraction : Term
    {
        public Abstraction(Variable variable, Term term)
        {
            AbstractVariable = variable;
            AbstractTerm = term;
        }

        public Variable AbstractVariable { get; private set; }
        public Term AbstractTerm { get; private set; }

        protected override string StringRepresentation
        {
            get { return @"\" + AbstractVariable + "." + AbstractTerm; }
        }

        public override bool IsNormal
        {
            get { return AbstractTerm.IsNormal; }
        }

        public override HashSet<Variable> Variables
        {
            get
            {
                HashSet<Variable> fv = AbstractTerm.Variables;
                fv.Remove(AbstractVariable);
                return fv;
            }
        }

        protected override bool CheckForInlineImpl(Term t, Term insteadOf,
            Dictionary<Abstraction, Variable> boundVariables)
        {
            boundVariables.Add(this, AbstractVariable);
            bool result = AbstractTerm.CheckForInline(t, insteadOf, boundVariables);
            boundVariables.Remove(this);
            return result;
        }

        public override Term TransformKCI()
        {
            if (AbstractTerm is Variable || AbstractTerm is Combinator)
                return AbstractVariable.Equals(AbstractTerm) ? (Term)ICombinator.I : new Application(KCombinator.K, AbstractTerm);
            if (AbstractTerm is Abstraction)
                return (new Abstraction(AbstractVariable, AbstractTerm.TransformKCI()).TransformKCI());
            if (AbstractTerm is Application)
            {
                var t = AbstractTerm as Application;
                var a = t.Left;
                var b = t.Right;
                return new Application(
                      new Application(SCombinator.S
                                  , new Abstraction(AbstractVariable, a).TransformKCI())
                    , new Abstraction(AbstractVariable, b).TransformKCI());
            }
            return null;
        }

        protected internal override Term ReduceImpl(Dictionary<Abstraction, Variable> boundVariables)
        {
            boundVariables.Add(this, AbstractVariable);
            var result = new Abstraction(AbstractVariable, AbstractTerm.ReduceImpl(new Dictionary<Abstraction, Variable>()));
            boundVariables.Remove(this);
            return result;
        }

        protected internal override Term SubstituteImpl(Variable v, Term with, Dictionary<Abstraction, Variable> boundVariables)
        {
            boundVariables.Add(this, AbstractVariable);
            var result = new Abstraction(AbstractVariable, AbstractTerm.SubstituteImpl(v, with, boundVariables));
            boundVariables.Remove(this);
            return result;
        }
    }

    public class Application : Term
    {
        public readonly Term Left;
        public readonly Term Right;

        public Application(Term left, Term right)
        {
            Left = left;
            Right = right;
        }

        protected override string StringRepresentation
        {
            get { return ((Left is Abstraction && !(Left is Combinator) ? "(" + Left + ")" : Left.ToString()) 
                       + " " 
                       + (Right is Application ? "(" + Right + ")" : Right.ToString())); }
        }

        public override HashSet<Variable> Variables
        {
            get
            {
                HashSet<Variable> fv = Left.Variables;
                fv.UnionWith(Right.Variables);
                return fv;
            }
        }

        public override bool IsNormal
        {
            get { return !(Left is Abstraction) && Left.IsNormal && Right.IsNormal; }
        }

        protected override bool CheckForInlineImpl(Term t, Term insteadOf,
            Dictionary<Abstraction, Variable> boundVariables)
        {
            return Left.CheckForInline(t, insteadOf, boundVariables) &&
                   Right.CheckForInline(t, insteadOf, boundVariables);
        }

        public override Term TransformKCI()
        {
            return new Application(Left.TransformKCI(), Right.TransformKCI());
        }

        protected internal override Term ReduceImpl(Dictionary<Abstraction, Variable> boundVariables)
        {
            Abstraction a;
            if ((a = Left as Abstraction) != null)
                return a.AbstractTerm.SubstituteImpl(a.AbstractVariable, Right, boundVariables);
            Term newLeft = Left.ReduceImpl(boundVariables);
            if (!newLeft.Equals(Left))
                return new Application(newLeft, Right);
            Term newRight = Right.ReduceImpl(boundVariables);
            return newRight.Equals(Right) 
                ? this 
                : new Application(Left, newRight);
        }

        protected internal override Term SubstituteImpl(Variable v, Term with, Dictionary<Abstraction, Variable> boundVariables)
        {
            var newLeft = Left.SubstituteImpl(v, with, boundVariables);
            var newRight = Right.SubstituteImpl(v, with, boundVariables);
            if (Equals(newLeft, Left) && Equals(newRight, Right))
                return this;
            return new Application(newLeft, newRight);
        }
    }
}