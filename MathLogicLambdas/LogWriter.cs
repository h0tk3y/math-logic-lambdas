﻿using System.IO;

namespace MathLogicLambdas
{
    public class LogWriter
    {
        private static StreamWriter sw = new StreamWriter("log.txt") {AutoFlush = true};

        public static void Write(string s)
        {
            sw.WriteLine(s);
        }
    }
}