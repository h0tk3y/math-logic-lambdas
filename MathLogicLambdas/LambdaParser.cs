﻿using System;
using System.Linq;

namespace MathLogicLambdas
{
    public class LambdaParser
    {
        private readonly string _str;
        private int _pos;

        private string Rem { get { return _str.Substring(_pos); } }    

        public static Term Parse(string expression)
        {
            var parser = new LambdaParser(expression);
            Term result = parser.ParseExpression();
            return parser._pos < expression.Length - 1
                ? null
                : result;
        }

        private LambdaParser(string expression)
        {
            _str = expression;
        }

        private Term ParseExpression()
        {
            Term left = ParseApplicationPart();
            while (TryConsume(" "))
                left = new Application(left, ParseApplicationPart());
            return left;
        }

        private Term ParseApplicationPart()
        {
            if (TryConsume(@"\"))
            {
                var v = ParseVariable();
                if (v == null)
                    throw new ParsingException(Rem);
                if (!TryConsume("."))
                    throw new ParsingException(Rem);
                var e = ParseExpression();
                return new Abstraction(v, e);
            }
            if (TryConsume("("))
            {
                var e = ParseExpression();
                if (!TryConsume(")"))
                    throw new ParsingException(Rem);
                return e;
            }
            var r = ParseVariable();
            if (r == null)
                throw new ParsingException(Rem);
            return r;
        }

        private Variable ParseVariable()
        {
            string name = ConditionalConsume(Char.IsLetter);
            return name == null ? null : new Variable(name);
        }

        private bool TryConsume(string what)
        {
            if (_pos + what.Length > _str.Length)
                return false;
            if (what.Where((t, i) => _str[_pos + i] != t).Any())
                return false;
            _pos += what.Length;
            return true;
        }

        private string ConditionalConsume(Predicate<char> condition)
        {
            int initialPos = _pos;
            while (_pos < _str.Length && condition(_str[_pos]))
                ++_pos;
            return _pos > initialPos ? _str.Substring(initialPos, _pos - initialPos) : null;
        }

        public class ParsingException : Exception
        {
            public string ProblemPart { get; private set; }

            private static string MakeMessage(string expressionPart)
            {
                return "Could not parse expression starting from " + expressionPart;
            }

            public ParsingException(string expressionPart)
                : base(MakeMessage(expressionPart))
            {
                ProblemPart = expressionPart;
            }
        }
    }
}